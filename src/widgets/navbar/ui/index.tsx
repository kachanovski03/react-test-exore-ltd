import React from "react";
import {Link} from "react-router-dom";
import styles from "./styles.module.css"

const Navbar = () => {
    return (
        <div className={styles.navbar}>
            <Link className={styles.link} to={"/"}>Главная</Link>
            <Link className={styles.link} to={"/products"}>Продукты</Link>
            <Link className={styles.link} to={"/product/create"}>Создать товар</Link>
        </div>
    )
}

export default Navbar
