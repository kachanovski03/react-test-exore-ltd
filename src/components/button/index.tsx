import React, {ButtonHTMLAttributes, DetailedHTMLProps, ReactNode} from 'react';
import styles from "./styles.module.css"

interface IProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    children: ReactNode,
}

export const Button = ({children, className, ...props}: IProps) => {
    return (
        <button className={styles.button}
                {...props}
        >
            {children}
        </button>
    );
};
