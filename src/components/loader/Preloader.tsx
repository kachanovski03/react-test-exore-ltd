import React from 'react';
import styles from './styles.module.css'

interface IProps {
    isFetching: boolean
}

export const Preloader = ({isFetching}: IProps) => {
    return (
        isFetching
            ? (
                <div className={styles.container}>
                    <div className={styles.loader}/>
                </div>
            )
            : null
    )
};
