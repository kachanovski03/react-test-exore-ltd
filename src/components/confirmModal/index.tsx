import React, {useState} from 'react';
import styles from "./styles.module.css"
import {Button} from "../index";

interface IProps {
    onPressConfirm: () => void
    buttonText: string
}

export const ButtonWithConfirm = ({onPressConfirm, buttonText}: IProps) => {
    const [showConfirm, setIsConfirm] = useState(false)

    const showModalHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        setIsConfirm(!showConfirm)
    }

    const onPressConfirmHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        onPressConfirm()
    }

    return (
        <>
            <Button onClick={(e) => showModalHandler(e)}>{buttonText}</Button>
            {showConfirm &&   <div className={styles.confirmModal}>
                <div className={styles.modalWrapper}>
                    <div className={styles.modal}>
                        <div>
                            You are sure???
                        </div>
                        <div>
                            <Button onClick={onPressConfirmHandler}>Yes</Button>
                            <Button onClick={showModalHandler}>No</Button>
                        </div>
                    </div>
                </div>
            </div>}
        </>
    );
};
