import {SET_IS_FETCHING, SET_STATUS} from "../constants";

export const commonActions = {
    setIsFetching: (isFetching: boolean) => ({type: SET_IS_FETCHING, payload: {isFetching}} as const),
    setStatus: (status: string) => ({type: SET_STATUS, payload: {status}} as const),
};
