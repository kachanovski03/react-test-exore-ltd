import {ICreatedProduct} from "../../interfaces";
import {SET_CREATED_PRODUCTS} from "../constants";

export const createdProductsActions = {
    setCreatedProducts: (createdProducts: ICreatedProduct[]) => ({
        type: SET_CREATED_PRODUCTS, payload: createdProducts
    }),
};
