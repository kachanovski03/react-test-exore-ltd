import {commonThunkCreator} from "../utils";
import {productsActions} from "../actions";
import {BaseThunkType, InferActionsType} from "../store";
import {productAPI} from "../../api";
import {IProduct} from "../../interfaces";

type ThunkType = BaseThunkType<InferActionsType<typeof productsActions>>;

export const getProducts = (limit?: number): ThunkType => async dispatch => {
    await commonThunkCreator(async () => {
        const res = await productAPI.getProducts(limit)
        dispatch(productsActions.setProducts(res.data))
    }, dispatch);
};
export const getProductById = (id: number): ThunkType => async dispatch => {
    await commonThunkCreator(async () => {
        const res = await productAPI.getProductById(id)
        dispatch(productsActions.setProduct(res.data))
    }, dispatch);
};

export const removeProduct = (id: number | string): ThunkType => async (dispatch, getState) => {
    await commonThunkCreator(async () => {
        const res = await productAPI.removeProduct(id)
        const store = getState();
        const filteredProducts = store.products.products.filter(i => i.id !== res.data.id)
        dispatch(productsActions.setProducts(filteredProducts))
    }, dispatch);
};


export const updateProduct = (id: number | string, product: IProduct): ThunkType => async (dispatch, getState) => {
    await commonThunkCreator(async () => {
        await productAPI.updateProduct(id, product)
    }, dispatch);
};


