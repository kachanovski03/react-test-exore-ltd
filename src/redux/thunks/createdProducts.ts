import {commonThunkCreator} from "../utils";
import {createdProductsActions} from "../actions";
import {BaseThunkType, InferActionsType} from "../store";
import {productAPI} from "../../api";
import {ICreatedProduct, ICreatedProductFields} from "../../interfaces";
import {getItemsFromLocalStorage, removeItemFromArrayLocalStorageById, saveArrayItemToLocalStorage} from "../../utils";

type ThunkType = BaseThunkType<InferActionsType<typeof createdProductsActions>>;

export const createProduct = (product: ICreatedProductFields): ThunkType => async (dispatch, getState) => {
    await commonThunkCreator(async () => {
        const res = await productAPI.createProduct(product)
        const newProduct: ICreatedProduct = {...product, id: res.data._id, createdAt: new Date()}
        saveArrayItemToLocalStorage("createdProduct", newProduct)
    }, dispatch);
};

export const getCreatedProducts = (key: string): ThunkType => async (dispatch, getState) => {
    await commonThunkCreator(async () => {
        const res: ICreatedProduct[] = getItemsFromLocalStorage(key) ?? []
        dispatch(createdProductsActions.setCreatedProducts(res))
    }, dispatch);
};

export const removeCreatedProduct = (key: string, id: string): ThunkType => async (dispatch, getState) => {
    await commonThunkCreator(async () => {
        const res: ICreatedProduct[] = removeItemFromArrayLocalStorageById(key, id)
        dispatch(createdProductsActions.setCreatedProducts(res))
    }, dispatch);
};


