import {BaseThunkType, InferActionsType} from "../store";
import {authActions} from "../actions";
import {commonThunkCreator} from "../utils";
import {authAPI} from "../../api";
import {IAuthForm} from "../../interfaces";

type ThunkType = BaseThunkType<InferActionsType<typeof authActions>>;

export const login = (data: IAuthForm): ThunkType => async dispatch => {
    await commonThunkCreator(async () => {
        await authAPI.login(data)
        dispatch(authActions.setIsAuth(true))
    }, dispatch);
};
