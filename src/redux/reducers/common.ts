import {InferActionsType} from "../store";
import {commonActions} from "../actions";
import {SET_IS_FETCHING, SET_STATUS} from "../constants";

export type IActions = InferActionsType<typeof commonActions>
export type ICommonReducer = typeof defaultState;

const defaultState = {
    isFetching: false,
    status: ""
}

export const commonReducer = (state = defaultState, action: IActions): ICommonReducer => {
    switch (action.type) {
        case SET_IS_FETCHING:
        case SET_STATUS:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
