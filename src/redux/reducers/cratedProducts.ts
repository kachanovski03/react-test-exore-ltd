import {InferActionsType} from "../store";
import {SET_CREATED_PRODUCTS} from "../constants";
import {ICreatedProduct} from "../../interfaces";
import {createdProductsActions} from "../actions";

export type IActions = InferActionsType<typeof createdProductsActions>
export type ICreatedProductReducer = typeof defaultState;

const defaultState = {
    createdProduct: [] as ICreatedProduct[],
}

export const createdProductsReducer = (state = defaultState, action: IActions): ICreatedProductReducer => {
    switch (action.type) {
        case SET_CREATED_PRODUCTS:
            return {
                ...state,
                createdProduct: action.payload
            }
        default:
            return state
    }
}
