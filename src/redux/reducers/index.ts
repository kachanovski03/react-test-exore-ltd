import {productsReducer} from "./products";
import {commonReducer} from "./common";
import {authReducer} from "./auth";

import type {IProductReducer} from "./products";
import type {IAuthReducer} from "./auth";
import type {ICommonReducer} from "./common";

export {
    productsReducer,
    commonReducer,
    authReducer
}

export type {
    IProductReducer,
    ICommonReducer,
    IAuthReducer
}
