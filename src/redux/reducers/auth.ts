import {InferActionsType} from "../store";
import {authActions} from "../actions/auth";
import { SET_IS_AUTH } from "../constants/auth";

export type IActions = InferActionsType<typeof authActions>
export type IAuthReducer = typeof defaultState;

const defaultState = {
    isAuth: false as boolean,
}

export const authReducer = (state = defaultState, action: IActions): IAuthReducer => {
    switch (action.type) {
        case SET_IS_AUTH:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
