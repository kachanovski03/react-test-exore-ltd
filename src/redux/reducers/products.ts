import {InferActionsType} from "../store";
import {productsActions} from "../actions";
import {SET_PRODUCT, SET_PRODUCTS} from "../constants";
import {IProduct} from "../../interfaces";

export type IActions = InferActionsType<typeof productsActions>
export type IProductReducer = typeof defaultState;

const defaultState = {
    products: [] as IProduct[],
    product: {} as IProduct,
}

export const productsReducer = (state = defaultState, action: IActions): IProductReducer => {
    switch (action.type) {
        case SET_PRODUCTS:
        case SET_PRODUCT:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}
