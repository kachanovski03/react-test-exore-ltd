import {instance} from "./instance";
import {ICreatedProductFields, IProduct} from "../interfaces";

export const productAPI = {
    getProducts(limit: number = 8) {
        return instance.get("products", {params: {limit: limit}})
    },
    getProductById(id: number) {
        return instance.get(`products/${id}`)
    },
    createProduct(product: ICreatedProductFields) {
        return instance.post(`products`, product)
    },
    updateProduct(id: string | number, product: IProduct) {
        return instance.put(`products/${id}`, product)
    },
    removeProduct(id: string | number) {
        return instance.delete(`products/${id}`,)
    }
}
