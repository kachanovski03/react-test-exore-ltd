import React from 'react';
import styles from "./styles.module.css"
import {Routes} from "../pages";
import {Header, Navbar} from "../widgets";
import {useSelector} from "react-redux";
import {RootStateType} from "../redux/store";
import {LoginPage} from "../pages/login";

function App() {
    const isAuth = useSelector<RootStateType, boolean>(state => state.auth.isAuth)

    if (!isAuth) {
        return <LoginPage/>
    }

    return (
        <div className={styles.app}>
            <Header/>
            <Navbar/>
            <Routes/>
        </div>
    );
}

export default App;
