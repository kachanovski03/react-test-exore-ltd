export const saveArrayItemToLocalStorage = (key: string, item: any) => {
    const items = localStorage.getItem(key)
    if (items) {
        const oldItems = JSON.parse(items)
        localStorage.setItem(key, JSON.stringify([...oldItems, item]))
    } else {
        localStorage.setItem(key, JSON.stringify([item]))
    }
}

export const saveItemToLocalStorage = (key: string, item: any) => {
    localStorage.setItem(key, JSON.stringify(item))
}


export const getItemsFromLocalStorage = (key: string) => {
    const items = localStorage.getItem(key)
    return items ? JSON.parse(items) : null
}

export const removeItemFromArrayLocalStorageById = (key: string, id: string) => {
    const items = localStorage.getItem(key)
    if (items) {
        const oldItems = JSON.parse(items)
        const filteredItems = oldItems.filter((i: any) => i.id !== id)
        localStorage.setItem(key, JSON.stringify(filteredItems))
        return filteredItems
    } else {
        return items
    }
}
