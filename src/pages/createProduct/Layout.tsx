import React from 'react';
import styles from "./styles.module.css"
import {useFormik} from "formik";
import { ICreatedProductFields } from '../../interfaces';

interface IProps {
    createProductHandler: (product: ICreatedProductFields) => void
}

export const Layout = ({createProductHandler}: IProps) => {

    const formik = useFormik<ICreatedProductFields>({
        initialValues: {
            title: '',
            price: 0,
            description: '',
            isPublished: false,
        },
        onSubmit: async (values, action) => {
            await createProductHandler({...values})
            action.resetForm()
        },
    })
    return (
        <div className={styles.content}>
            <h1>Создание продукта</h1>
            <form className={styles.form} onSubmit={formik.handleSubmit}>
                <label htmlFor="title">Название</label>
                <input
                    required
                    id="title"
                    name="title"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.title}
                />
                <label htmlFor="price">Цена</label>
                <input
                    required
                    id="price"
                    name="price"
                    type="number"
                    onChange={formik.handleChange}
                    value={formik.values.price}
                />
                <label htmlFor="description">Описание</label>
                <input
                    required
                    id="description"
                    name="description"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                />
                <label htmlFor="isPublished">Опубликован</label>
                <input
                    id="isPublished"
                    name="isPublished"
                    type="checkbox"
                    onChange={formik.handleChange}
                    checked={formik.values.isPublished}
                />

                <button type="submit">
                    Создать продукт
                </button>
            </form>
        </div>
    );
};
