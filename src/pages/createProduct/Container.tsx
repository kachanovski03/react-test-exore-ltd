import React from 'react';
import {Layout} from "./Layout";
import {useDispatch} from "react-redux";
import {createProduct} from "../../redux/thunks";
import {ICreatedProductFields} from "../../interfaces";

export const Container = () => {
    const dispatch = useDispatch()

    const createProductHandler = (product: ICreatedProductFields) => {
       dispatch(createProduct(product))
    }

    return (
        <Layout  createProductHandler={createProductHandler}/>
    );
};
