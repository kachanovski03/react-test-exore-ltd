import React from "react";
import styles from "./styles.module.css"
import {ProductsPage} from "./products"
import {ProductPage} from "./product"
import {NotFoundPage} from "./notFound"
import {MainPage} from "./main"
import {Route, Routes as Switch} from "react-router-dom"
import {CreateProductPage} from "./createProduct";
import {Preloader} from "../components";
import {useSelector} from "react-redux";
import {RootStateType} from "../redux/store";
import { UpdateProductPage } from "./updateProduct";

export const Routes = () => {
    const isFetching = useSelector<RootStateType, boolean>(state => state.common.isFetching)
    return (
        <div className={styles.pageContent}>
            <Preloader isFetching={isFetching}/>
            <Switch>
                <Route path={'/'} element={<MainPage/>}/>
                <Route path="/products" element={<ProductsPage/>}/>
                <Route path="/product/:id" element={<ProductPage/>}/>
                <Route path="/product/create" element={<CreateProductPage/>}/>
                <Route path="/product/edit/:id" element={<UpdateProductPage/>}/>
                <Route path="*" element={<NotFoundPage/>}/>
            </Switch>
        </div>

    )
}
