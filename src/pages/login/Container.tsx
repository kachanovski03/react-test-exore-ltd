import React from 'react';
import {Layout} from './Layout';
import {useDispatch} from "react-redux";
import {IAuthForm} from "../../interfaces";
import {login} from "../../redux/thunks";

export const Container = () => {
    const dispatch = useDispatch()

    const loginHandler = (data: IAuthForm) => {
        dispatch(login(data))
    }

    return (
        <Layout loginHandler={loginHandler}/>
    )
};
