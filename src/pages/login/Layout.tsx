import React from 'react';
import styles from "./styles.module.css"
import {useFormik} from "formik";
import {IAuthForm} from "../../interfaces";

interface IProps {
    loginHandler: (data: IAuthForm) => void
}

export const Layout = ({loginHandler}: IProps) => {

    const formik = useFormik<IAuthForm>({
        initialValues: {
            username: 'mor_2314',
            password: "83r5^_",
        },
        onSubmit: values => {
            loginHandler(values)
        },
    })

    return (
        <div className={styles.content}>
            <h1>Войти</h1>
            <form className={styles.form} onSubmit={formik.handleSubmit}>
                <label htmlFor="username">Логин</label>
                <input
                    required
                    id="username"
                    name="username"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.username}
                />
                <label htmlFor="password">Пароль</label>
                <input
                    required
                    id="password"
                    name="password"
                    type="password"
                    onChange={formik.handleChange}
                    value={formik.values.password}
                />
                <button type="submit">
                    войти
                </button>
            </form>
        </div>

    );
};
