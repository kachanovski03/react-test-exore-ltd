import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {Layout} from './Layout';
import {useDispatch, useSelector} from "react-redux";
import {RootStateType} from "../../redux/store";
import {IProduct} from "../../interfaces";
import {getProductById} from "../../redux/thunks";
import {productsActions} from "../../redux/actions";

export const Container = () => {
    const {id} = useParams();
    const dispatch = useDispatch()
    const product = useSelector<RootStateType, IProduct>(state => state.products.product)
    useEffect(() => {
        if (id) {
            dispatch(getProductById(Number(id)))
        }
        return () => {
            dispatch(productsActions.setProduct({} as IProduct))
        }
    }, [dispatch, id])

    return (
        <Layout product={product}/>
    );
};
