import React from 'react';
import {IProduct} from "../../interfaces";

interface IProps {
    product: IProduct
}

export const Layout = ({product}: IProps) => {
    return (
        <div>
           <img alt={""} src={product.image} />
            <div>
                <div>
                    {product.title}
                </div>
                <div>
                    {product.category}
                </div>
                <div>
                    {product.description}
                </div>
                <div>
                    {product.price}
                </div>
            </div>
        </div>
    );
};
