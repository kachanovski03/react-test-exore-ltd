import React from 'react';
import styles from "./styles.module.css"
import {IProduct} from "../../interfaces";
import {useFormik} from 'formik';
import {Button, ButtonWithConfirm} from "../../components";

interface IProps {
    removeItemHandler: () => void
    updateProductHandler: (product: IProduct) => void
    product: any
}

export const Layout = ({updateProductHandler, removeItemHandler, product}: IProps) => {

    const formik = useFormik({
        initialValues: {
            title: product.title || "",
            price: product.price || "",
            description: product.description || "",
            category: product.category || "",
        },
        enableReinitialize: true,
        onSubmit: async (values) => {
            await updateProductHandler({...product, ...values})
        },
    });
    return (
        <div className={styles.content}>
            <h1>Изменение продукта</h1>
            <form className={styles.form} onSubmit={formik.handleSubmit}>
                <label htmlFor="title">title</label>
                <input
                    required
                    id="title"
                    name="title"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.title}
                />
                <label htmlFor="price">price</label>
                <input
                    required
                    id="price"
                    name="price"
                    type="number"
                    onChange={formik.handleChange}
                    value={formik.values.price}
                />
                <label htmlFor="description">description</label>
                <input
                    required
                    id="description"
                    name="description"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                />
                <label htmlFor="category">category</label>
                <input
                    id="category"
                    name="category"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.category}
                />
                <Button type="submit">
                    Сохранить
                </Button>
            </form>
            <ButtonWithConfirm buttonText={"Удалить"} onPressConfirm={removeItemHandler}/>
        </div>
    );
};
