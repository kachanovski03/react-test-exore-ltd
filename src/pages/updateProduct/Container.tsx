import React, {useEffect} from 'react';
import {Layout} from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {IProduct} from "../../interfaces";
import {useNavigate, useParams} from "react-router-dom";
import {getProductById, removeProduct, updateProduct} from '../../redux/thunks';
import {RootStateType} from "../../redux/store";

export const Container = () => {
    const {id} = useParams()
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const product = useSelector<RootStateType, IProduct>(state => state.products.product)
    useEffect(() => {
        if (id) {
            dispatch(getProductById(+id))
        }
    }, [dispatch, id])

    const updateProductHandler = (product: IProduct) => {
        if (id) {
            dispatch(updateProduct(id, product))
        }
    }

    const removeItemHandler = () => {
        if (id) {
            dispatch(removeProduct(id))
            navigate("/products")
        }
    }

    return (
        <Layout removeItemHandler={removeItemHandler} product={product} updateProductHandler={updateProductHandler}/>
    );
};
