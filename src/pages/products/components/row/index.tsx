import React from 'react';
import styles from "./styles.module.css"
import {ButtonWithConfirm} from "../../../../components";

interface IProps {
    id: string
    title: string
    price: number
    description: string
    isPublished: boolean
    removeItemHandler: (id: string) => void
}

export const Row = ({id, title, price, isPublished, description, removeItemHandler}: IProps) => {
    const removeItem = () => {
        removeItemHandler(id)
    }
    return (
        <div className={styles.rows}>
            <div className={styles.row}>
                {title}
            </div>
            <div className={styles.row}>
                {price}
            </div>
            <div className={styles.row}>
                {description}
            </div>
            <div className={styles.row}>
                {isPublished ? "Yes" : "No"}
            </div>
            <ButtonWithConfirm onPressConfirm={removeItem} buttonText={"удалить"} />
        </div>
    );
};
