import React, {useEffect, useState} from 'react';
import styles from "./styles.module.css"
import {useDispatch, useSelector} from "react-redux";
import {RootStateType} from "../../../../redux/store";
import {ICreatedProduct} from "../../../../interfaces";
import {getCreatedProducts, removeCreatedProduct} from '../../../../redux/thunks';
import {Row} from "../row";
import {useFormik} from "formik";
import {filterCreatedProducts} from '../../../../redux/utils';
import {CREATED_PRODUCT_FILTER_TYPE} from "../../../../constants";

export const CreatedProductsList = () => {
    const dispatch = useDispatch()
    const [filterType, setFilterType] = useState<CREATED_PRODUCT_FILTER_TYPE>(CREATED_PRODUCT_FILTER_TYPE.ALL)
    const createdProducts = useSelector<RootStateType, ICreatedProduct[]>(state => state.createdProducts.createdProduct)

    const formik = useFormik({
        initialValues: {
            isPublished: CREATED_PRODUCT_FILTER_TYPE.ALL,
        },
        onSubmit: (values) => {
            setFilterType(values.isPublished)
        },
    });

    useEffect(() => {
        dispatch(getCreatedProducts("createdProduct"))
    }, [dispatch])

    const removeItemHandler = (id: string) => {
        dispatch(removeCreatedProduct("createdProduct", id))
    }

    if (createdProducts?.length === 0) {
        return <div>Пока ничего нет</div>
    }

    return (
        <div>
            <div className={styles.columns}>
                <div>title</div>
                <div>price</div>
                <div>description</div>
                <div>isPublished</div>
                <div>Actions</div>
            </div>
            <div>
                <h3>Filters</h3>
                <form className={styles.form} onSubmit={formik.handleSubmit}>
                    <label htmlFor="isPublished">isPublished</label>
                    <select
                        name="isPublished"
                        value={formik.values.isPublished}
                        onChange={(e) => {
                            formik.handleSubmit()
                            formik.handleChange(e)
                        }

                           }
                    >
                        <option value={CREATED_PRODUCT_FILTER_TYPE.ALL} label="все" />
                        <option value={CREATED_PRODUCT_FILTER_TYPE.PUBLISHED} label="опубликованные" />
                        <option value={CREATED_PRODUCT_FILTER_TYPE.UNPUBLISHED} label="неопубликованные" />
                    </select>
                </form>
            </div>
            <div>
                {filterCreatedProducts(createdProducts, filterType).map(i => <Row description={i.description}
                                               isPublished={i.isPublished}
                                               id={i.id}
                                               key={i.id}
                                               title={i.title}
                                               price={i.price}
                                               removeItemHandler={removeItemHandler}/>
                )}
            </div>
        </div>
    )
};
