import React from 'react';
import styles from "./styles.module.css"
import {Button, ButtonWithConfirm} from "../../../../components";
import {useNavigate} from "react-router-dom";

interface IProps {
    id: number
    image: string
    title: string
    price: number
    removeItemHandler: (id: number) => void
    updateItemHandler: (id: number) => void
}

export const Card = ({id, image, price, title, removeItemHandler, updateItemHandler}: IProps) => {
    const navigate = useNavigate()
    const removeItem = () => {
        removeItemHandler(id)
    }
    const updateItem = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        navigate(`/product/edit/${id}`, {replace: true});
        updateItemHandler(id)
    }

    return (
        <div className={styles.card}>
            <img alt={"item_img"} className={styles.image} src={image}/>

            <div className={styles.title}>
                {title}
            </div>
            <div className={styles.price}>
                {price}
            </div>
            <ButtonWithConfirm onPressConfirm={removeItem} buttonText={"удалить"} />
            <Button onClick={(e) => updateItem(e)}>Редактировать</Button>
        </div>
    );
};
