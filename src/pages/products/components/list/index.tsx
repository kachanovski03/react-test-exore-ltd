import React, {ChangeEvent, useEffect, useState} from 'react';
import styles from "./styles.module.css"
import {useDispatch, useSelector} from "react-redux";
import {RootStateType} from "../../../../redux/store";
import {IProduct} from "../../../../interfaces";
import {getProducts, removeProduct, updateProduct} from "../../../../redux/thunks";
import {Card} from "../card";
import {Button} from "../../../../components";
import {Link} from "react-router-dom";
import {productsActions} from "../../../../redux/actions";

export const List = () => {
    const dispatch = useDispatch()

    const [searchValue, setSearchValue] = useState<string>("")

    const products = useSelector<RootStateType, IProduct[]>(state => state.products.products)
    useEffect(() => {
        dispatch(getProducts())
        return () => {
            dispatch(productsActions.setProducts([]))
        }
    }, [dispatch])

    const getValueByLimit = (limit: number) => {
        dispatch(getProducts(limit))
    }

    const removeItemHandler = (id: number) => {
        dispatch(removeProduct(id))
    }
    const updateItemHandler = (id:number) => {
        dispatch(updateProduct(id, {} as IProduct))
    }

    const onChangeSearchValue = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchValue(e.currentTarget.value)
    }

    return (
        <div className={styles.list}>
            <Button onClick={() => getValueByLimit(8)}>8</Button>
            <Button onClick={() => getValueByLimit(16)}>16</Button>
            <Button onClick={() => getValueByLimit(20)}>20</Button>

            <div>
                <span>Поиск</span>
                <input value={searchValue} onChange={onChangeSearchValue} placeholder={"Поиск"}/>
            </div>
            <div className={styles.content}>
                {products.filter(p => p.title.toLowerCase().includes(searchValue.toLowerCase())).map(i => <Link key={i.id} className={styles.link} to={`/product/${i.id}`}>
                        <Card id={i.id} removeItemHandler={removeItemHandler} updateItemHandler={updateItemHandler}
                              image={i.image} title={i.title} price={i.price}/>
                    </Link>
                )}
            </div>
        </div>
    );
};
