import React, {useEffect, useState} from 'react';
import {Layout} from "./Layout";
import {getItemsFromLocalStorage, saveItemToLocalStorage} from "../../utils";
import { FILTER_TYPE_PRODUCTS_PAGE } from '../../constants';

export const Container = () => {
    const [filterType, setFilterType] = useState<FILTER_TYPE_PRODUCTS_PAGE>(FILTER_TYPE_PRODUCTS_PAGE.SERVER)

    useEffect(() => {
        const type = getItemsFromLocalStorage("filterType") ?? FILTER_TYPE_PRODUCTS_PAGE.SERVER
        setFilterType(type)
    }, [])

    const changeFilterTypeHandler = (type: FILTER_TYPE_PRODUCTS_PAGE) => {
        saveItemToLocalStorage("filterType", type)
        setFilterType(type)
    }
    return (
        <Layout filterType={filterType} changeFilterTypeHandler={changeFilterTypeHandler}/>
    );
};
