import React from 'react';
import styles from "./styles.module.css"
import {CreatedProductsList} from "./components";
import {List} from './components';
import {FILTER_TYPE_PRODUCTS_PAGE} from '../../constants';

interface IProps {
    filterType: FILTER_TYPE_PRODUCTS_PAGE
    changeFilterTypeHandler: (type: FILTER_TYPE_PRODUCTS_PAGE) => void;
}

export const Layout = (props: IProps) => {
    const {changeFilterTypeHandler, filterType} = props
    return (
        <>
            <div className={styles.tabs}>
                <span onClick={() => changeFilterTypeHandler(FILTER_TYPE_PRODUCTS_PAGE.SERVER)}
                      className={`${styles.tab} ${filterType === FILTER_TYPE_PRODUCTS_PAGE.SERVER && styles.activeTab}`}
                >
                    {FILTER_TYPE_PRODUCTS_PAGE.SERVER}
                </span>
                <span
                    className={`${styles.tab} ${filterType === FILTER_TYPE_PRODUCTS_PAGE.CREATED && styles.activeTab}`}
                    onClick={() => changeFilterTypeHandler(FILTER_TYPE_PRODUCTS_PAGE.CREATED)}
                >
                    {FILTER_TYPE_PRODUCTS_PAGE.CREATED}
                </span>
            </div>

            <div>
                {
                    filterType === FILTER_TYPE_PRODUCTS_PAGE.SERVER
                        ? <List/>
                        : <CreatedProductsList/>
                }
            </div>
        </>
    );
};
